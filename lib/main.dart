import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:fsamplepisatel/core/bloc/bloc.dart';
import 'package:fsamplepisatel/sl.dart';

import 'ui/pages/home_page/home_page.dart';

void main() {
  EquatableConfig.stringify = true;
  setupGetIt();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).whenComplete(() async {
    runApp(App());
  });

}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Пример постов и деталей',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Посты'),
        ),
        body: BlocProvider(
          create: (context) => PostBloc()..add(PostFetched()),
          child: HomePage(),
        ),
      ),
    );
  }
}
