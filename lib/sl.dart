import 'package:fsamplepisatel/core/repository/repository.dart';
import 'package:http/http.dart' as http;
import 'package:get_it/get_it.dart';
import 'package:path_provider/path_provider.dart';

import 'core/repository/local/local_data_provider.dart';
import 'core/repository/remote/remote_data_provider.dart';

final getIt = GetIt.instance;

void setupGetIt() {
  getIt.registerSingletonAsync<RemoteDataProvider>(
      () async => RemoteDataProvider(httpClient: http.Client()));
  getIt.registerSingletonAsync<LocalDataProvider>(() async =>
      LocalDataProvider((await getApplicationDocumentsDirectory()).path));
  getIt.registerSingletonWithDependencies<Repository>(() => Repository(),
      dependsOn: [RemoteDataProvider, LocalDataProvider]);
}

// ignore: non_constant_identifier_names
final TheRepository = getIt<Repository>();
