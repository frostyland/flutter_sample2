import 'package:flutter/material.dart';
import 'package:fsamplepisatel/core/models/post.dart';

class PostListItem extends StatelessWidget {
  final Post post;
  final Function onTap;

  const PostListItem({this.post, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                  blurRadius: 3.0,
                  offset: Offset(0.0, 2.0),
                  color: Color.fromARGB(80, 0, 0, 0))
            ]),
        child: ListTile(
          leading: (post.thumbnailUri ?? '').isEmpty
              ? Container(
                  child: Text(''),
                )
              : CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.transparent,
                  backgroundImage: NetworkImage(post.thumbnailUri),
                ),
          title: Text(
            post.title, maxLines: 1, overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.w900, fontSize: 14.0),
          ),
          subtitle:
              Text(post.body, maxLines: 2, overflow: TextOverflow.ellipsis),
          trailing: Text('${post.id}'),
        ),
      ),
    );
  }
}
