import 'dart:convert';

import 'package:fsamplepisatel/core/common/utils.dart';
import 'package:fsamplepisatel/core/models/comment.dart';
import 'package:fsamplepisatel/core/models/post.dart';
import 'package:fsamplepisatel/core/repository/data_provider.dart';
import 'package:hive/hive.dart';

class LocalDataProvider extends DataProvider {
  final String path;
  final _db = Hive;
  final _postBoxName = 'posts';
  final _commentsBoxName = 'comments';

  LocalDataProvider(this.path) {
    _db.init(path);
  }

  @override
  Future<Post> getPost(int id) async {
//    print('LocalDataProvider.getPost $id');
    final map = (await _db.openBox(_postBoxName)).get(id);
    if (map == null) {
      return null;
    }
//    print('$now: LocalDataProvider.getPost: id = ${map['id']}');
    return Post(
        id: map['id'],
        title: map['title'],
        body: map['body'],
        thumbnailUri: map['thumbnailUri']);
  }

  void savePost(Post post) async {
//    print('LocalDataProvider.savePost: post=$post');
    (await _db.openBox(_postBoxName)).put(post.id, post.toJson());
  }

  @override
  Future<List<Comment>> getCommentsForPost(int postId) async {
    var comments = List<Comment>();
    final lst = (await _db.openBox(_commentsBoxName)).get(postId);
    if (lst == null) {
      return null;
    }
    var decoded = json.decode(lst) as List;
    for (var comment in decoded) {
      comments.add(Comment.fromJson(comment));
    }
    return comments;
  }

  void saveComments(int postId, List<Comment> comments) async {
    final j = json.encode(comments);
    (await _db.openBox(_commentsBoxName)).put(postId, j);
  }
}
