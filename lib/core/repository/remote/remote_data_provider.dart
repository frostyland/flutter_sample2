import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:fsamplepisatel/core/models/comment.dart';
import 'package:http/http.dart' as http;
import 'package:fsamplepisatel/core/models/post.dart';
import 'package:fsamplepisatel/core/repository/data_provider.dart';
import 'package:rxdart/rxdart.dart';

class RemoteDataProvider extends DataProvider {
  final http.Client httpClient;

  RemoteDataProvider({@required this.httpClient});

  @override
  Future<Post> getPost(int id) async {
    final s1 = DeferStream(() => Stream.fromFuture(_getPost(id)));
    final s2 = DeferStream(() => Stream.fromFuture(_getImage(id)));
    final s = s1.zipWith(s2, (t, s) {
      if (t == null) return null;
      final post = Post(
        id: t['id'],
        title: t['title'],
        body: t['body'],
        thumbnailUri: s['url'] ?? '',
      );
      return post;
    });
    return s.single;
  }

  Future<Map<String, dynamic>> _getPost(int id) async {
    final response = await httpClient
        .get('https://jsonplaceholder.typicode.com/posts?id=$id');
    var map = <String, dynamic>{};
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      if (data.length == 0) {
        return null;
      }
      map = data[0];
    } else {
      throw Exception('error fetching posts');
    }
    return map;
  }

  Future<Map<String, dynamic>> _getImage(int id) async {
    final response = await httpClient.get(
        'https://jsonplaceholder.typicode.com/albums/1/photos?id=${id % 25}');
    var map = <String, dynamic>{};
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      if (data.length == 0) {
      } else {
        map = data[0];
      }
    }
    return map;
  }

  @override
  Future<List<Comment>> getCommentsForPost(int postId) async {
    var comments = List<Comment>();

    // Get comments for post
    var response = await httpClient
        .get('https://jsonplaceholder.typicode.com/comments?postId=$postId');

    // Parse into List
    var parsed = json.decode(response.body) as List<dynamic>;

    // Loop and convert each item to a Comment
    for (var comment in parsed) {
      comments.add(Comment.fromJson(comment));
    }

    return comments;
  }
}
