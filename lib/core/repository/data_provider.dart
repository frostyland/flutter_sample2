import 'package:fsamplepisatel/core/models/comment.dart';
import 'package:fsamplepisatel/core/models/post.dart';

abstract class DataProvider {
  Future<Post> getPost(int id);

  Future<List<Comment>> getCommentsForPost(int postId);
}
