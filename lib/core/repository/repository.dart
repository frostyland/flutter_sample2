import 'package:fsamplepisatel/core/common/utils.dart';
import 'package:fsamplepisatel/core/models/comment.dart';
import 'package:fsamplepisatel/core/models/post.dart';
import 'package:fsamplepisatel/core/repository/local/local_data_provider.dart';
import 'package:fsamplepisatel/core/repository/remote/remote_data_provider.dart';
import 'package:fsamplepisatel/sl.dart';

class Repository {
  final LocalDataProvider local = getIt<LocalDataProvider>();
  final RemoteDataProvider remote = getIt<RemoteDataProvider>();

  Future<List<Post>> fetchPosts(int startIndex, int limit) async {
    final result = <Post>[];
    final upper = startIndex + limit;
    for (var i = startIndex; i < upper; ++i) {
      var item = await local.getPost(i);
      if (item == null) {
        item = await remote.getPost(i);
        if (item != null) {
          local.savePost(item);
        }
      }
      if (item != null) {
        result.add(item);
      }
    }

    return result;
  }

  Future<List<Comment>> fetchComments(int postId) async {
    final result = <Comment>[];
    var items = await local.getCommentsForPost(postId);
    if (items == null) {
      items = await remote.getCommentsForPost(postId);
      if (items != null) {
        local.saveComments(postId, items);
      }
    }
    if (items != null) {
      result.addAll(items);
    }
    return result;
  }
}
