import 'package:equatable/equatable.dart';

///
class Post extends Equatable {
  final int id;
  final String title;
  final String body;
  final String thumbnailUri;

  const Post({this.id, this.title, this.body, this.thumbnailUri});

  @override
  List<Object> get props => [id, title, body];

  Map<String, dynamic> toJson() {
    final data = Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['body'] = this.body;
    data['thumbnailUri'] = this.thumbnailUri;
    return data;
  }
}
