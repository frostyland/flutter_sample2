import 'dart:io';

/// Simple show of current time
DateTime get now => DateTime.now();

/// Current time with period calculation
/// if one need to calculate periods between some events,
/// start with [Now.start]
/// and proceed with [Now.elapsed]
///
/// ### Example
///
///     print('${Now.start}: event1');
///     print('${Now.elapsed}: event2');
///     print('${Now.elapsed}: end');
///
///     // output will like
///     2020-07-21 20:15:53.403158 (start): event1
///     2020-07-21 20:16:08.173796 (+14770): event2
///     2020-07-21 20:16:08.174347 (+14771): end
class NowImpl {
  int _startMs = 0;
  int _accumulatorMs = 0;

  String get start => _start();

  String get elapsed => _elapsed();

  String _start() {
    final t = DateTime.now();
    _startMs = t.millisecondsSinceEpoch;
    _accumulatorMs = 0;
    return '$t (start)';
  }

  String _elapsed() {
    final t = DateTime.now();
    if (_startMs == 0) {
      _startMs = t.millisecondsSinceEpoch;
    }
    _accumulatorMs = t.millisecondsSinceEpoch - _startMs;
    return '$t (+$_accumulatorMs)';
  }

  NowImpl() {
    _start();
  }
}

// ignore: non_constant_identifier_names
final Now = NowImpl();

Future delayMs(int value) => Future.delayed(Duration(milliseconds: value));

Future delayMk(int value) => Future.delayed(Duration(microseconds: value));

Future delaySec(int value) => Future.delayed(Duration(seconds: value));

/// Deletes directory.
/// Beware of deleting root directory, so check path yoy pass in this function!!
Future delDir(String path, {bool recursive = false, String errMsg}) async {
  final dir = Directory(path);
  if (await dir.exists()) {
    try {
      dir.deleteSync(recursive: recursive ?? false);
    } catch (e) {
      print('${errMsg ?? 'delDir(path)'}: $e');
    }
  }
}

/// Checks if path exists for [Directory], [File] or [Link]
bool pathNotFound(String path) =>
    FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound;
