import 'package:equatable/equatable.dart';

abstract class CommentsEvent extends Equatable {
  final int postId;

  CommentsEvent([this.postId]);

  @override
  List<Object> get props => [postId];
}

class CommentsFetched extends CommentsEvent {
  CommentsFetched([int postId]) : super(postId);
}
