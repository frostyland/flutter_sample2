import 'package:bloc/bloc.dart';
import 'package:fsamplepisatel/core/bloc/comments_event.dart';
import 'package:fsamplepisatel/core/bloc/comments_state.dart';
import 'package:fsamplepisatel/core/models/comment.dart';
import 'package:fsamplepisatel/sl.dart';
import 'package:rxdart/rxdart.dart' as rx;
import 'package:fsamplepisatel/core/bloc/bloc.dart';

///
class CommentsBloc extends Bloc<CommentsEvent, CommentsState> {

  CommentsBloc() : super(CommentsInitial());

  @override
  Stream<CommentsState> mapEventToState(CommentsEvent event) async* {
    final currentState = state;
    if (event is CommentsFetched) {
      try {
        if (currentState is CommentsInitial) {
          final comments = await _fetchComments(event.postId);
          yield CommentsSuccess(comments: comments);
          return;
        }
      } catch (_) {
        yield CommentsFailure(null);
      }
    }
  }

  @override
  Stream<Transition<CommentsEvent, CommentsState>> transformEvents(
    Stream<CommentsEvent> events,
    TransitionFunction<CommentsEvent, CommentsState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 150)),
      transitionFn,
    );
  }

  Future<List<Comment>> _fetchComments(int postId) async {
    return await TheRepository.fetchComments(postId);
  }
}
