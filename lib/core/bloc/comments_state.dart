import 'package:equatable/equatable.dart';
import 'package:fsamplepisatel/core/models/comment.dart';
import 'package:fsamplepisatel/core/models/post.dart';

abstract class CommentsState extends Equatable {
  final int postId;

  const CommentsState(this.postId);

  @override
  List<Object> get props => [];
}

class CommentsInitial extends CommentsState {
  CommentsInitial() : super(null);
}

class CommentsFailure extends CommentsState {
  CommentsFailure(int postId) : super(postId);
}

class CommentsSuccess extends CommentsState {
  final List<Comment> comments;

  const CommentsSuccess({
    this.comments,
    int postId,
  }) : super(postId);

  CommentsSuccess copyWith({
    List<Post> posts,
    int postId,
  }) {
    return CommentsSuccess(
      comments: comments ?? this.comments,
      postId: postId ?? this.postId,
    );
  }

  @override
  List<Object> get props => [postId, comments];
}
