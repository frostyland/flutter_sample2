import 'package:fsamplepisatel/core/common/utils.dart';
import 'package:rxdart/rxdart.dart';

import 'package:flutter_test/flutter_test.dart';

void main() {
  /// Here we test 2 features:
  /// 1. [Rx.defer] as lazy implementation of streams
  /// 2. zipping 2 results together
  test('zip 2 streams', () async {
    print(': zip 2 streams');
    final arr = [10, 20];
    print('$now: prepare');
    final s1 = DeferStream(() => Stream.fromFuture(makeResulter(arr[0])));
    final s2 = DeferStream(() => Stream.fromFuture(makeResulter(arr[1])));
    await delayMs(200);
    print('$now: start');
    await s1.zipWith(s2, (t, s) => t.value + s.value).single.then(expectAsync1((v) {
      print('$now: zipped: v=$v');
      expect(v, equals(arr.reduce((v, e) => e + v)));
    }));
    print('$now: finish');
  });

  test('gens', () async {
    print(': gens');
    final arr1 = ['a', 'b', 'c'];
    print('create iterator');
    final numbers = DeferStream(() => makeString(arr1));
//    Stream<int> numbers = getNumbers(3);
    await delayMs(1000);
    print('starting to listen...');
    numbers.listen((value) {
      print('$value');
    });
    await delayMs(1000);
    print('end of main');
  });

  test('zip 2 streams 2', () async {
    print(': zip 2 streams 2');
    final arr1 = ['a', 'b', 'c'];
    final arr = [10, 20, 30];
    print('$now: prepare');
    final s1 = DeferStream(() => makeString(arr1));
    final s2 = DeferStream(() => makeInt(arr));
    await delayMs(200);
    print('$now: start');
    s1.zipWith(s2, (t, s) => '$t + $s').listen((event) {
      print('$now: zipped: v=$event');
    });
    await delayMs(10000);
    print('$now: finish');
  });

}

Stream<String> makeString(List<String> input) async*{
  for(var i=0; i<input.length; ++i){
    await delayMs(10);
    yield input[i];
  }
}

Stream<int> makeInt(List<int> input) async*{
  for(var i=0; i<input.length; ++i){
    await delayMs(1000*(i+1));
    yield input[i];
  }
}

class Resulter {
  final int value;

  Resulter(this.value) {
    print('$now: Resulter.Resulter: $value');
  }
}

Future<Resulter> makeResulter(int value) async {
  await delayMs(value);
  return Resulter(value);
}
