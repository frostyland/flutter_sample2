# Тестовый проект

Приложение с двумя экранами, на первом - список постов, где отображается картинка и текст поста. По клику на пост переход на следующий экран, где вверху находится картинка поста, поверх нее название, ниже тело поста, еще ниже список комментов.
Картинки согласно ТЗ на разработку, чисто для заполнения.

Паттерны `BLoC`, `Repository` with local cache, `Clean Architecture`, `infinite scroll`, `shimmer`, `rxdart`, etc.


## Getting Started

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
